EESchema Schematic File Version 4
LIBS:circuit-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L devkit:ESP32-DEVKITC-32D U2
U 1 1 6021C18B
P 5250 4050
F 0 "U2" H 5250 5217 50  0000 C CNN
F 1 "ESP32-DEVKITC-32D" H 5250 5126 50  0000 C CNN
F 2 "esp:MODULE_ESP32-DEVKITC-32D" H 5250 4050 50  0001 L BNN
F 3 "" H 5250 4050 50  0001 L BNN
F 4 "4" H 5250 4050 50  0001 L BNN "PARTREV"
F 5 "Espressif Systems" H 5250 4050 50  0001 L BNN "MANUFACTURER"
	1    5250 4050
	1    0    0    -1  
$EndComp
Entry Wire Line
	7000 4550 6900 4650
Text Label 6200 4650 0    50   ~ 0
sck
Entry Wire Line
	7000 4250 6900 4350
Text Label 6200 4350 0    50   ~ 0
sda
$Comp
L Sensor_Optical:LDR03 R1
U 1 1 6021DE39
P 4650 2400
F 0 "R1" H 4720 2446 50  0000 L CNN
F 1 "LDR03" H 4720 2355 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" V 4825 2400 50  0001 C CNN
F 3 "http://www.elektronica-componenten.nl/WebRoot/StoreNL/Shops/61422969/54F1/BA0C/C664/31B9/2173/C0A8/2AB9/2AEF/LDR03IMP.pdf" H 4650 2350 50  0001 C CNN
	1    4650 2400
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R2
U 1 1 6021D83D
P 5150 2400
F 0 "R2" H 5220 2446 50  0000 L CNN
F 1 "R" H 5220 2355 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P2.54mm_Vertical" V 5080 2400 50  0001 C CNN
F 3 "~" H 5150 2400 50  0001 C CNN
	1    5150 2400
	0    -1   -1   0   
$EndComp
Entry Wire Line
	7450 2800 7550 2700
Entry Wire Line
	7350 2800 7450 2700
Entry Wire Line
	7250 2800 7350 2700
Entry Wire Line
	7150 2800 7250 2700
Text Label 7550 2700 1    50   ~ 0
sda
Text Label 7450 2700 1    50   ~ 0
sck
Text Label 7250 2700 1    50   ~ 0
gnd
Text Label 7350 2700 1    50   ~ 0
vdd
$Comp
L Connector:Conn_01x04_Male J1
U 1 1 6021FE1D
P 7450 2400
F 0 "J1" V 7512 2544 50  0000 L CNN
F 1 "Display" V 7603 2544 50  0000 L CNN
F 2 "Connector_JST:JST_XH_B4B-XH-A_1x04_P2.50mm_Vertical" H 7450 2400 50  0001 C CNN
F 3 "~" H 7450 2400 50  0001 C CNN
	1    7450 2400
	0    1    1    0   
$EndComp
Entry Wire Line
	7000 3650 6900 3750
Entry Wire Line
	4350 2950 4450 3050
Text Label 6150 3750 0    50   ~ 0
gnd
Text Label 4450 3100 0    50   ~ 0
vdd
$Comp
L Switch:SW_Push SW1
U 1 1 6024775C
P 6450 2450
F 0 "SW1" H 6450 2735 50  0000 C CNN
F 1 "SW_Push" H 6450 2644 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 6450 2650 50  0001 C CNN
F 3 "~" H 6450 2650 50  0001 C CNN
	1    6450 2450
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x05_Female J2
U 1 1 6024C6BA
P 1900 5100
F 0 "J2" V 1838 4812 50  0000 R CNN
F 1 "radar" V 1747 4812 50  0000 R CNN
F 2 "Connector_JST:JST_EH_B5B-EH-A_1x05_P2.50mm_Vertical" H 1900 5100 50  0001 C CNN
F 3 "~" H 1900 5100 50  0001 C CNN
	1    1900 5100
	0    -1   -1   0   
$EndComp
Entry Wire Line
	4100 5050 4200 4950
Entry Wire Line
	4100 4650 4200 4550
Entry Wire Line
	4100 4550 4200 4450
Text Label 4250 4450 0    50   ~ 0
gnd
Text Label 4250 4950 0    50   ~ 0
5v
Text Label 4200 4550 0    50   ~ 0
rdata
Entry Wire Line
	1900 5550 1800 5450
Entry Wire Line
	2000 5550 1900 5450
Entry Wire Line
	2100 5550 2000 5450
Text Label 1800 5450 1    50   ~ 0
gnd
Text Label 1900 5450 1    50   ~ 0
rdata
Text Label 2000 5450 1    50   ~ 0
5v
Text Notes 7850 1850 2    50   ~ 0
se puede poner la temperatura del BMP280 en el bus, ojo que dice que va a 5v
Text Label 8100 2700 1    50   ~ 0
gnd
Text Label 8200 2700 1    50   ~ 0
vdd
$Comp
L Connector:Conn_01x04_Male J3
U 1 1 6023AA12
P 8300 2400
F 0 "J3" V 8362 2544 50  0000 L CNN
F 1 "bme680" V 8453 2544 50  0000 L CNN
F 2 "Connector_JST:JST_XH_B4B-XH-A_1x04_P2.50mm_Vertical" H 8300 2400 50  0001 C CNN
F 3 "~" H 8300 2400 50  0001 C CNN
	1    8300 2400
	0    1    1    0   
$EndComp
$Comp
L Switch:SW_Push SW2
U 1 1 60245EF4
P 3100 2600
F 0 "SW2" H 3100 2885 50  0000 C CNN
F 1 "SW_Push" H 3100 2794 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 3100 2800 50  0001 C CNN
F 3 "~" H 3100 2800 50  0001 C CNN
	1    3100 2600
	1    0    0    -1  
$EndComp
Text Label 2750 2600 0    50   ~ 0
gnd
Text Notes 6100 2350 0    50   ~ 0
no se puso al final
Wire Wire Line
	6050 4650 6900 4650
Wire Wire Line
	6050 4350 6900 4350
Wire Wire Line
	5300 2400 5400 2400
Wire Wire Line
	7550 2700 7550 2600
Wire Wire Line
	7450 2700 7450 2600
Wire Wire Line
	7350 2700 7350 2600
Wire Wire Line
	7250 2700 7250 2600
Wire Bus Line
	4350 2800 7000 2800
Wire Wire Line
	4450 3050 4450 3150
Wire Wire Line
	6050 3750 6900 3750
Wire Wire Line
	4450 3250 3300 3250
Wire Wire Line
	3300 3250 3300 2600
Wire Wire Line
	4200 4550 4450 4550
Wire Wire Line
	4200 4450 4450 4450
Wire Wire Line
	4200 4950 4450 4950
Wire Wire Line
	1800 5300 1800 5450
Wire Wire Line
	1900 5300 1900 5450
Wire Wire Line
	2000 5300 2000 5450
Wire Wire Line
	6650 2450 6650 3250
Wire Wire Line
	6650 3250 6050 3250
Wire Wire Line
	6250 2450 6250 3150
Wire Wire Line
	6250 3150 6050 3150
Wire Wire Line
	8200 2700 8200 2600
Wire Wire Line
	8100 2700 8100 2600
Wire Wire Line
	4450 3650 4300 3650
Wire Wire Line
	4300 3650 4300 2650
Wire Wire Line
	4900 2400 5000 2400
Wire Wire Line
	4800 2400 4900 2400
Connection ~ 4900 2400
Wire Wire Line
	4900 2150 4900 2400
Wire Wire Line
	4250 3750 4450 3750
Wire Wire Line
	4300 2650 5450 2650
Wire Wire Line
	5450 2650 5450 2150
Wire Wire Line
	5450 2150 4900 2150
Wire Wire Line
	4250 2600 4250 3750
Wire Wire Line
	4250 2600 5400 2600
Wire Wire Line
	5400 2400 5400 2600
Wire Wire Line
	4500 2400 4500 2550
Wire Wire Line
	4500 2550 4200 2550
Wire Wire Line
	4200 3850 4450 3850
Wire Wire Line
	4200 2550 4200 3850
Wire Wire Line
	2900 2600 2750 2600
Connection ~ 7000 2800
Wire Wire Line
	8400 2600 8400 3650
Wire Wire Line
	8300 2600 8300 3350
Wire Wire Line
	8300 3350 6050 3350
Wire Bus Line
	4350 2800 4350 3150
Wire Wire Line
	6050 3650 8400 3650
Wire Bus Line
	4100 4450 4100 5550
Wire Bus Line
	7000 2800 7000 4650
Wire Bus Line
	1650 5550 4100 5550
Wire Bus Line
	7000 2800 7550 2800
$EndSCHEMATC
