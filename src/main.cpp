#include <Arduino.h>
#include "EspNow2MqttGateway.hpp"
#include "secrets.hpp"
#include "displayTask.hpp"
#include <WiFi.h>
#include <WiFiClient.h>
#include "OTAUtil.hpp"
#include "radar.h"
#include "sensors.hpp"

//ota constants
#define OTA_RETRIES 50 // > 5s
#define HOTA_HOSTNAME "espnowgw"

unsigned int messageCounter = 0;
WiFiClient wifiClient;
EspNow2MqttGateway gw = EspNow2MqttGateway(sharedKey, wifiClient, mqtt_server, 1883, sharedChannel);
bool radarValue;
sensorReadsT sensorReads;
char sensorReadsJson[200];

void displayMyMac(){
    char macStr[22];
    strcpy(macStr, "Mac ");
    strcat(macStr,WiFi.macAddress().c_str());
    Serial.println(macStr);
}

void setupWiFi(const char* ssid, const char* password){
    WiFi.mode(WIFI_MODE_STA);
    WiFi.setSleep(false);
    WiFi.begin(ssid, password, sharedChannel); 
    displayLine(DISPLAY_LINE_WIFI_AND_MQTT,"wifiConnect ", true);
    while (WiFi.status() != WL_CONNECTED) { // Wait for the Wi-Fi to connect
        delay(100);
        displayLine(DISPLAY_LINE_WIFI_AND_MQTT, "trying to connect to wifi.."); 
    }
    displayLine(DISPLAY_LINE_WIFI_AND_MQTT,"Connection established!");
    String ipMsg =  String("ip ");
    ipMsg.concat( WiFi.localIP().toString());
    ipMsg.concat( " ch ");
    ipMsg.concat( String((int) WiFi.channel()) );
    displayLine(DISPLAY_LINE_WIFI_AND_MQTT, ipMsg.c_str());  
}

unsigned int mqttCounter = 0;
void displayMqttIncomingData(char* topic, byte* payload, unsigned int length){
    char line[DISPLAY_CHARACTERS_X_LINE + 1];
    snprintf(line, sizeof(line), "%i/%i<=%i#%s",
        gw.getNumberOfMessages(),
        gw.getNumberOfSubscriptions(),
        mqttCounter ++,
        topic + 7*sizeof(char) );
    displayLine(DISPLAY_LINE_WIFI_AND_MQTT, line);
}

void displayRequestAndResponse(bool ack, request &rq, response &rsp ){
    char line[DISPLAY_CHARACTERS_X_LINE + 1];
    for (int opCount = 0; opCount < rq.operations_count; opCount ++)
    {
        int lineNum = DISPLAY_LINE_OPERATIONS + opCount;
        int responseCode;
        if (rsp.opResponses_count>opCount){
                responseCode = rsp.opResponses[opCount].result_code;
            } else {
                responseCode = -1;
            }
        switch (rq.operations[opCount].which_op)
        {
        case request_Operation_ping_tag:
            snprintf(line, sizeof(line), "ping: %d,%i", rq.operations[opCount].op.ping.num, responseCode );
            break;
        case request_Operation_send_tag:
            snprintf(line, sizeof(line), "send: %s <%i", rq.operations[opCount].op.send.queue, responseCode );
            break;
        case request_Operation_qRequest_tag:
            snprintf(line, sizeof(line), "ask: %s >%i", rq.operations[opCount].op.qRequest.queue, responseCode);
            break;
        default:
            snprintf(line, sizeof(line), "unknown op");
            break;
        }
        displayLine(lineNum,line,false);
    }
    for (int lineToClear = DISPLAY_LINE_OPERATIONS + rq.operations_count +1 ; lineToClear < DISPLAY_LINE_WIFI_AND_MQTT ; lineToClear++){
        displayLine(lineToClear, "", false);
    }
    snprintf(line, sizeof(line), "#%i %s t%dops%i",
        messageCounter ++,
        rq.client_id,
        rq.message_type, 
        rq.operations_count);
    displayLine(DISPLAY_LINE_IN,line,true);
    String ipMsg =  String("ip ");
    ipMsg.concat( WiFi.localIP().toString());
    ipMsg.concat( " ch ");
    ipMsg.concat( String((int) WiFi.channel()) );
    displayLine(DISPLAY_LINE_WIFI_AND_MQTT, ipMsg.c_str(),false);
    snprintf(line, sizeof(line), "%i/%i",
        gw.getNumberOfMessages(),
        gw.getNumberOfSubscriptions());
    displayLine(DISPLAY_LINE_WIFI_AND_MQTT, line);
}

void setup() {
  Serial.begin(115200);
  xTaskCreate(displayTaskF, "displayTask", 15000, NULL, 1, NULL);

  displayMyMac();
  setupWiFi(ssid, password);
  InitOTA(HOTA_HOSTNAME);
  gw.init();
  gw.onProcessedRequest = displayRequestAndResponse;
  gw.onMqttDataReceived = displayMqttIncomingData;
  EspNow2Mqtt_subscribe(); 
  //radar
  attachInterrupt(GPIO_RADAR, ISR_radar, CHANGE);
  //sensores
  xTaskCreate(sensorTaskF, "sensorsTask", 15000, NULL, 1, NULL);
}

void loop() {
  delay(50);
  
  //mqtt
  gw.loop(); //required to fetch messages from mqtt
  
  //ota
  ArduinoOTA.handle();
  
  //radar
  if (pdTRUE == xQueueReceive(qRadar, &radarValue, 0)){
    char strSend[6];
    strcpy(strSend, (digitalRead(GPIO_RADAR) == HIGH) ? "true":"false");
    gw.sendGwMqttMessage("radar", strSend);
  }
  
  //sensors
  if ( pdTRUE == xQueueReceive(qSensors, &sensorReads, 0)){
      sensors2Json(sensorReads, sensorReadsJson, sizeof(sensorReadsJson));
      gw.sendGwMqttMessage("sensors", sensorReadsJson);
  }
}