#ifndef _DISPLAY_HPP_
#define _DISPLAY_HPP_

#include "constants.h"

//#include <U8x8lib.h>
#include <U8g2lib.h>


#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif

//#define DISPLAY_TYPE_TEXT

#ifdef DISPLAY_TYPE_TEXT
U8X8_SSD1306_128X64_NONAME_SW_I2C u8x8(/* clock=*/ GPIO_I2C_CLOCK, /* data=*/ GPIO_I2C_DATA, /* reset=*/ DISPLAY_RESET); 
#else
U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(U8G2_R0, /* clock=*/ GPIO_I2C_CLOCK, /* data=*/ GPIO_I2C_DATA, /* reset=*/ DISPLAY_RESET);
#endif

class Display {
  public:
    int lineH = 9;
    bool serialCopy;
    Display (bool serialCopy = false) {
        this->serialCopy = serialCopy;
    }
    void init() {
        #ifdef DISPLAY_TYPE_TEXT
        u8x8.begin();
        u8x8.setPowerSave(0);
        u8x8.setFont(u8x8_font_chroma48medium8_r);
        #else
        u8g2.begin();
        u8g2.setFont(u8g2_font_6x10_tr);
        u8g2.setDisplayRotation(U8G2_R2);
        u8g2.clearBuffer();
        #endif
    }
    void print(int linea, const char * s, bool refresh=true) {
        #ifdef DISPLAY_TYPE_TEXT
        u8x8.drawString(0, linea ,"               " );
        u8x8.drawString(0, linea ,s );
        u8x8.refreshDisplay();		// only required for SSD1606/7  
        #else
        int linePX = lineH * linea;
        u8g2.setDrawColor(0);
        u8g2.drawBox(0,linePX-lineH, 128, lineH+1);
        u8g2.setDrawColor(1);
        u8g2.drawStr(0,linePX,s);
        if (refresh) u8g2.sendBuffer();
        if (serialCopy){
            Serial.println(s);
        }
        #endif
    }

};

#endif

