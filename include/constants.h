#ifndef _CONSTANS_H_
#define _CONSTANS_H_

//gpio pins
#define GPIO_RADAR 13

//i2c
#define GPIO_I2C_CLOCK 15
#define GPIO_I2C_DATA 4

//display
#define DISPLAY_RESET 16

//sensores
#define SENSOR_PERIOD_MS 10*1000

//sensor luz
#define GPIO_R_LIGHT 35 //gpio 35 / ch1a07 

#endif