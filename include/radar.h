#ifndef _RADAR_H_
#define _RADAR_H_

#include "constants.h"
#include <Arduino.h>
#include <freertos/queue.h>

QueueHandle_t qRadar = xQueueCreate(2, sizeof(boolean));

//ISR routine
void IRAM_ATTR ISR_radar (){
    bool payload = digitalRead(GPIO_RADAR) == HIGH;
    BaseType_t changedContext;
    xQueueSendToBackFromISR(qRadar, &payload, &changedContext);
    return;
}


#endif