#ifndef _DISPLAY_TASK_HPP_
#define _DISPLAY_TASK_HPP_

#include "display.hpp"
#include <freertos/task.h>
#include <freertos/queue.h>

//display constans
#define DISPLAY_LINE_IN 1
#define DISPLAY_LINE_OPERATIONS 2
#define DISPLAY_LINE_WIFI_AND_MQTT 7
#define DISPLAY_CHARACTERS_X_LINE 20

struct displayMsgT {
    int lineIndex;
    char payload[DISPLAY_CHARACTERS_X_LINE +1];
    bool refresh = false;
};

QueueHandle_t qDisplay = xQueueCreate(15, sizeof(displayMsgT));

void displayTaskF(void * args){
    
    //setup
    Display display = Display(false);
    display.init();
    displayMsgT msg;

    //loop
    for(;;){
        if (pdTRUE == xQueueReceive(qDisplay, &msg, 10000)){
            display.print(msg.lineIndex, msg.payload, msg.refresh);
        } else {
            Serial1.print('>');
        }
    }
}

void displayLine(int linea, const char * s, bool refresh=true) {
    displayMsgT msg;
    msg.lineIndex = linea ;
    strncpy(msg.payload, s, DISPLAY_CHARACTERS_X_LINE);
    msg.refresh = refresh;
    xQueueSendToBack(qDisplay, &msg, 0);
}

#endif