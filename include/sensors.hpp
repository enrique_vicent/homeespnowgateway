#ifndef _SENSORS_HPP_
#define _SENSORS_HPP_

#include "constants.h"
#include <Arduino.h>
#include <freertos/task.h>
#include <freertos/queue.h>
#include "analogSensor.hpp"
#include "bsec.h"

// Create an object of the class Bsec
Bsec iaqSensor;

struct sensorReadsT {
    int light;
    float rawTemperature;
    float temperature;
    float presure;
    float humidity;
    float rawHumidity;
    float iaq;
    int iaqAccuracy;
    float staticIaq;
    float co2;
    float breathVoc;
    bool bmeRead;
    float gas;
};

QueueHandle_t qSensors = xQueueCreate(3, sizeof(sensorReadsT));
unsigned long nextRead,now = millis();

// Helper functions declarations
void checkIaqSensorStatus(void);
void setupIaqSensor ();
bool readIaqSensor(sensorReadsT &sensorReads);


void sensorTaskF(void * args){
    //setup
    AnalogSensor light(GPIO_R_LIGHT);
    sensorReadsT sensorReads;
    setupIaqSensor();
    //loop
    for(;;){
        bool bmeread = readIaqSensor(sensorReads);
        if (bmeread) {
            sensorReads.light = light.read(20);
            now = millis();
            if(now > nextRead){
                xQueueSendToBack(qSensors, &sensorReads, 0);
                nextRead = now + SENSOR_PERIOD_MS;
            } // else ignore read
        }
        delay(10);
    }   
}

void sensors2Json (sensorReadsT &reads, char*json, int jsonLenght = 100){
    unsigned long time = millis();
    if(reads.bmeRead){ //TODO: poner mas datos hasta entenderlos (precisión por ejemplo)
        snprintf(json, jsonLenght -1, "{\"luz\":%d,\"timestamp\":%ld,\"temp\":%.2f,\"hum\":%.1f,\"pres\":%.0f,\"iaq\":%.2f,\"staticIaq\":%.2f,\"iaqAccuracy\":%ld,\"bVoc\":%.2f,\"Co2\":%.0f,\"gas\":%.0f}", 
            reads.light,
            time,
            reads.temperature,
            reads.humidity,
            reads.presure,
            reads.iaq,
            reads.staticIaq,
            reads.iaqAccuracy,
            reads.breathVoc,
            reads.co2,
            reads.gas
            );
    } else {
        snprintf(json, jsonLenght -1, "{\"luz\":%d,\"timestamp\":%d}", 
            reads.light,
            time
            );
    }
}


void setupIaqSensor (){
    Wire.begin();//default values: sda21 scl22
    iaqSensor.begin(BME680_I2C_ADDR_SECONDARY, Wire);
    checkIaqSensorStatus();
    bsec_virtual_sensor_t sensorList[10] = {
        BSEC_OUTPUT_RAW_TEMPERATURE,
        BSEC_OUTPUT_RAW_PRESSURE,
        BSEC_OUTPUT_RAW_HUMIDITY,
        BSEC_OUTPUT_RAW_GAS,
        BSEC_OUTPUT_IAQ,
        BSEC_OUTPUT_STATIC_IAQ,
        BSEC_OUTPUT_CO2_EQUIVALENT,
        BSEC_OUTPUT_BREATH_VOC_EQUIVALENT,
        BSEC_OUTPUT_SENSOR_HEAT_COMPENSATED_TEMPERATURE,
        BSEC_OUTPUT_SENSOR_HEAT_COMPENSATED_HUMIDITY,
    };
    iaqSensor.updateSubscription(sensorList, 10, BSEC_SAMPLE_RATE_ULP);
    checkIaqSensorStatus();
}

void checkIaqSensorStatus(void)
{
    String output;
    if (iaqSensor.status != BSEC_OK) {
        if (iaqSensor.status < BSEC_OK) {
            output = "BSEC error code : " + String(iaqSensor.status);
            Serial.println(output);
        } else {
            output = "BSEC warning code : " + String(iaqSensor.status);
            Serial.println(output);
        }
    }

    if (iaqSensor.bme680Status != BME680_OK) {
        if (iaqSensor.bme680Status < BME680_OK) {
            output = "BME680 error code : " + String(iaqSensor.bme680Status);
            Serial.println(output);
        } else {
            output = "BME680 warning code : " + String(iaqSensor.bme680Status);
            Serial.println(output);
        }
    }
}

bool readIaqSensor(sensorReadsT &sensorReads){
    if (iaqSensor.run()) { // If new data is available
        sensorReads.rawTemperature = iaqSensor.rawTemperature;
        sensorReads.presure = iaqSensor.pressure;
        sensorReads.rawHumidity = iaqSensor.rawHumidity;
        sensorReads.gas = iaqSensor.gasResistance;
        sensorReads.iaq = iaqSensor.iaq;
        sensorReads.iaqAccuracy = iaqSensor.iaqAccuracy;
        sensorReads.temperature = iaqSensor.temperature;
        sensorReads.humidity = iaqSensor.humidity;
        sensorReads.staticIaq = iaqSensor.staticIaq;
        sensorReads.co2 = iaqSensor.co2Equivalent;
        sensorReads.breathVoc = iaqSensor.breathVocEquivalent;
        sensorReads.bmeRead = true;
        return true;
    } else {
        sensorReads.bmeRead = false;
        checkIaqSensorStatus();
        return false;
    }
}

#endif